/* Drop tables */
DROP TABLE Users;
DROP TABLE Device;
DROP TABLE Model;
DROP TABLE ActionType;
DROP TABLE Action;
DROP TABLE Rule;

/* Create tables */
CREATE TABLE Users(
uid INT AUTO_INCREMENT,
name VARCHAR(30) NOT NULL,
pw CHAR(40) NOT NULL,   /*field size is fixed and its length depends on the type of hashing we use; may update accordingly*/
ifActive BOOLEAN,
Email CHAR(30) NOT NULL,
PRIMARY KEY(uid)
);

CREATE TABLE Model(
modelID INT AUTO_INCREMENT,
brand VARCHAR(20) NOT NULL,
model VARCHAR(30) NOT NULL,
modelType VARCHAR(20) NOT NULL,
PRIMARY KEY(modelID)
);

CREATE TABLE Device(
did INTEGER AUTO_INCREMENT,
uid INT,
modelID INT,
serialID VARCHAR(20),
name VARCHAR(30) NOT NULL,
PRIMARY KEY (did),
FOREIGN KEY (uid) REFERENCES Users(uid) ON DELETE CASCADE,
FOREIGN KEY (modelID) REFERENCES Model(modelID) ON DELETE CASCADE
 );

CREATE TABLE ActionType(
modelID INT,
modelActionTypeID INT AUTO_INCREMENT,
modelActionType VARCHAR(20),
PRIMARY KEY (modelActionTypeID),
FOREIGN KEY (modelID) REFERENCES Model(modelID) ON DELETE CASCADE
);

CREATE TABLE Action(
actionID INTEGER AUTO_INCREMENT,
deviceID INTEGER,
modelActionTypeID INT,
FOREIGN KEY (deviceID) REFERENCES Device(did) ON DELETE CASCADE,
FOREIGN KEY (modelActionTypeID) 
REFERENCES ActionType(modelActionTypeID) ON DELETE CASCADE,
PRIMARY KEY (actionID)
);

CREATE TABLE Rule(
rid INTEGER PRIMARY KEY,
name VARCHAR(20),
operators VARCHAR(5) NOT NULL,
conditionOP1 VARCHAR(2),
conditionType1 VARCHAR(10),
conditionPara1 VARCHAR(10),
conditionOP2 VARCHAR(2),
conditionType2 VARCHAR(10),
conditionPara2 VARCHAR(10),
conditionOP3 VARCHAR(2),
conditionType3 VARCHAR(10),
conditionPara3 VARCHAR(10),
actionID INT NOT NULL,
FOREIGN KEY (actionID) REFERENCES Action(actionID)
);


