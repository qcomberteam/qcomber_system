package com.execute;

import com.model.Rule;

public class RuleExecutor implements Runnable{
	private Rule rule;
	
	public RuleExecutor (Rule rule){
		this.rule = rule;
	}
	
	@Override
	public void run() {
		boolean value = true;
		
		if (rule.hasNonTimeBaseConditions()){
			if (!rule.evaluateNonTimeBaseConditions()){
				value = false;
			}
		}
		
		if (value) {
			rule.activate();
		} else {
			rule.standby();
		}
	}
}
