package com.execute;

import com.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.RejectedExecutionException;

public class Main {
	public static final int MIN_WORKER_NUMBER = 5;
	public static final int MAX_WORKER_NUMBER = 10;
	public static final long WORKER_KEEP_ALIVE_TIME_IN_MILLIS = 300*1000;
	public static final TimeUnit WORKER_KEEP_ALIVE_TIME_UNIT = TimeUnit.MILLISECONDS;
	
	public static ThreadPoolExecutor executor;
	public static BlockingQueue<Runnable> queue;
	
	public static TimeBaseConditionPipe TBCPipe;
	
	public static void main(String[] args) {
//        test();
		queue = new ArrayBlockingQueue<Runnable>(MAX_WORKER_NUMBER);
		executor = new ThreadPoolExecutor(MIN_WORKER_NUMBER, MAX_WORKER_NUMBER, WORKER_KEEP_ALIVE_TIME_IN_MILLIS, WORKER_KEEP_ALIVE_TIME_UNIT, queue);
		
		// TODO dummy data
		DeviceModel model = new DeviceModel("SkyNet", "TX1000", null);
		Device d = new Device(model, "5AD96F", 0);
		ActionType at = new ActionType(model, "dummyAction");
		List<Device> deviceList = new ArrayList<Device>();
        List<Action> actionList = new ArrayList<Action>();
        List<Rule> ruleList = new ArrayList<Rule>();
		
		TBCPipe = new TimeBaseConditionPipe();
		
		for (int j = 0; j < 20; j++){
			deviceList.add(new Device(model, "MachineGun #"+j, 0));
		}
		
		for (int j = 0; j < 20; j++){
			actionList.add(new Action(at, deviceList.get(j)));
		}
		
		for (int j = 0; j < 20; j++){
            List<Condition> conditions = new ArrayList<Condition>();
            Rule rule = new Rule (conditions, null, actionList.get(j), "FoundJohnConner "+j);

			if (System.currentTimeMillis() % 2 == 0) {
				TimeBaseCondition c = new TimeBaseCondition("tbc"+j, rule);
				rule.addCondition(c);
                System.out.println("Rule " + j + " has TBC with next time: " + c.getNextActivationTimeInMillies());
			}
            
			ruleList.add(new Rule(conditions, null, actionList.get(j), "FoundJohnConner "+j));
		}
        //filtered out the rules with TBCs
        filterRuleList(ruleList);

		//Done generating dummy data
		System.out.println("done making up data");
		int i = 0;
//		while (true){
//			try{
//				System.out.println("Throwing rule "+i+" into the pool");
//				executor.execute(new RuleExecutor(ruleList.get(i)));
////				System.out.println("Rule "+i+" before i++");
//				i++;
////				System.out.println("Rule "+(i-1)+" after i++");
//			} catch (RejectedExecutionException e){
//				System.out.println("Rule " + i +" The worker pool is full.. sleeping for bit now");
//				try{
//					Thread.sleep(3000);
//				} catch (InterruptedException ie) {
//		            ie.printStackTrace();
//		        }
//			}
//			if (i >= ruleList.size()){
//				i = 0;
//			}
//		}
		
		TimeBaseCondition nextTBC;
		//this loop works for TBC rule only 
//		while (true){
//			nextTBC = TBCPipe.getNext();
//			if (nextTBC != null){
//				try {
//                    System.out.println("Main: Throwing rule " + nextTBC.getRule().getMyName() + " into the pool");
//                    executor.execute(new RuleExecutor(nextTBC.getRule()));
//				} catch (RejectedExecutionException e){
//					System.out.println("Main: Rule " + i +" The worker pool is full.. sleeping for bit now");
//					try{
//						Thread.sleep(3000);
//					} catch (InterruptedException ie) {
//			            ie.printStackTrace();
//			        }
//				}
//			}
//		}

        int index = 0;
        while (true){
            nextTBC = TBCPipe.getNext();
            if (nextTBC != null){
                ruleList.add(nextTBC.getRule());
            }
            if (index < ruleList.size()){
                try {
                    System.out.println("Main: Throwing rule " + ruleList.get(index).getMyName() + " into the pool");
                    executor.execute(new RuleExecutor(ruleList.get(index)));
                } catch (RejectedExecutionException e){
                    System.out.println("Main: Rule " + i +" The worker pool is full.. sleeping for bit now");
                    try{
                        Thread.sleep(3000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
                index++;
            } else {
                index = 0;
            }
        }
		
	}

    public static void filterRuleList(List<Rule> ruleList){
        for (int i = ruleList.size() - 1; i >= 0; i--){
            if (ruleList.get(i).hasTimeBaseConditions()){
                ruleList.remove(i);
            }
        }
    }

    //TODO i know this is not the best way, but for now, whatever..
    public static void test(){
        List<Long> list = new ArrayList<Long>();
//        for (long i=0; i<10000000; i++){
//        for (long i=0; i<100000; i++){
//        for (long i=0; i<100000; i=i+5){
//        for (long i=0; i<10000000; i=i+5){
        for (long i=0; i<1; i=i+5){
            list.add(i);
        }
        int result = testBinarySearch(list, 73456);
        System.out.println("b-search result: "+result);
    }

    private static int testBinarySearch(List<Long> list, long time){
        int position = list.size()/2;
        long closestMatch = list.get(position);
        int halfInterval = position;
        long previousDiff = -1;
        long newDiff = -1;
        int numberOfIteration = 0;
        while (true){
            numberOfIteration++;
            halfInterval = halfInterval/2;
            if (halfInterval < 1){
                //when the halfInterval is approaching 1, use finer search method
                halfInterval = 1;
            }

            //this is the finner search method
            if (halfInterval == 1){
                newDiff = Math.abs(closestMatch-time);
            }

            if (closestMatch < time){
                position = position + halfInterval;
            } else {
                position = position - halfInterval;
            }

            //make sure things are within bound
            if (position >= list.size()){
                position = list.size() - 1;
            } else if (position < 0){
                position = 0;
            }

            closestMatch = list.get(position);

            //this is the finner search method
            if (halfInterval == 1){
                if (previousDiff != -1){
                    if (newDiff >= previousDiff){
                        System.out.println(
                                "Found! Total list item: "
                                    + list.size()
                                    + ", number of iteration: "
                                    + numberOfIteration);
                        return position;
                    } else {
                        previousDiff = newDiff;
                    }
                } else {
                    previousDiff = newDiff;
                }
            }
        }
    }

}