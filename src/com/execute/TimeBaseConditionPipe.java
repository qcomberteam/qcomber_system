package com.execute;

import java.util.ArrayList;
import java.util.List;

import com.model.TimeBaseCondition;

public class TimeBaseConditionPipe {
	List<TimeBaseCondition> TBCList;
	public TimeBaseConditionPipe(){
		TBCList = new ArrayList<TimeBaseCondition>();
	}
	
	public synchronized void add(TimeBaseCondition c){
		if (!c.isInPipe()) {
            if (TBCList.isEmpty()){
                TBCList.add(c);
                return;
            }
            int position = binarySearch(c.getNextActivationTimeInMillies());
			c.setIsInPipe(true);
            //note that the binarySearch algorithm finds the position of the best match c
            //not the to-be-inserted position. As a result, in order to figure out where to
            //insert, do a simple check to find out..
            if (TBCList.get(position).getNextActivationTimeInMillies() > c.getNextActivationTimeInMillies()){
                TBCList.add(position, c);
            } else {
                TBCList.add(position+1, c);
            }
		}
	}
	
	//no need for this for now..
	public synchronized TimeBaseCondition pop(){
		if (TBCList.size()>0){
			try{
				return TBCList.get(0);
			}finally{
				TBCList.remove(0);
			}
		}
		return null;			
	}

    private int binarySearch(long time){
        int position = TBCList.size()/2;
        long closestMatch = TBCList.get(position).getNextActivationTimeInMillies();
        int halfInterval = position;
        long previousDiff = -1;
        long newDiff = -1;
        int numberOfIteration = 0;
        while (true){
            numberOfIteration++;
            halfInterval = halfInterval/2;
            if (halfInterval < 1){
                //when the halfInterval is approaching 1, use finer search method
                halfInterval = 1;
            }

            //this is the finner search method
            if (halfInterval == 1){
                newDiff = Math.abs(closestMatch-time);
            }

            if (closestMatch < time){
                position = position + halfInterval;
            } else {
                position = position - halfInterval;
            }

            //make sure things are within bound
            if (position >= TBCList.size()){
                position = TBCList.size() - 1;
            } else if (position < 0){
                position = 0;
            }

            closestMatch = TBCList.get(position).getNextActivationTimeInMillies();

            //this is the finner search method
            if (halfInterval == 1){
                if (previousDiff != -1){
                    if (newDiff >= previousDiff){
                        System.out.println(
                                "Found! Total list item: "
                                        + TBCList.size()
                                        + ", number of iteration: "
                                        + numberOfIteration);
                        return position;
                    } else {
                        previousDiff = newDiff;
                    }
                } else {
                    previousDiff = newDiff;
                }
            }
        }
    }

	private int linearSearch(long time){
		int i = 0;
		for (i = 0; i < TBCList.size(); i++){
			if (TBCList.get(i).getNextActivationTimeInMillies() > time){
				return i;
			}
		}
		return i;
	}
	
	private synchronized TimeBaseCondition getFirst(){
        if (TBCList.isEmpty()){
            return null;
        }
		return TBCList.get(0);
	}
	
	private synchronized void removeFirst(){
		TimeBaseCondition c = TBCList.remove(0);
		c.setIsInPipe(false);
	}
	
	public boolean isNextReady(){
		TimeBaseCondition c = getFirst();
		return c.onEvaluate();
	}
	
	public TimeBaseCondition getNext(){
		TimeBaseCondition c = getFirst();
		if (c==null){
			return null;
		}
		if (c.onEvaluate()){
			System.out.println("Main: done eval "+c.getRule().getMyName());
			removeFirst();
			if (c.getNextActivationTimeInMillies() > 0){
				add(c);
			}
			return c;
		}
		return null;
	}
	
}
