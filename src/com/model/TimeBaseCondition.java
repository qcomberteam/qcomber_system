package com.model;

/**
 * Created by Rane on 2015-08-20.
 */
public class TimeBaseCondition extends Condition{
    private long nextActivationTimeInMillies;
    private boolean isRecurring;
    private Rule rule;
    private boolean isInPipe;
    
    public TimeBaseCondition (String param, Rule rule){
        super(param);
        //TODO the following construction is for framework building purpose only
        calculateNextActivationTime();
        isRecurring = true;
        this.rule = rule;
        isInPipe = false;
    }

    public long calculateNextActivationTime(){
        //TODO make proper calculation code
        //for framework building purpose, setting next act time to be 5 seconds after calling
        nextActivationTimeInMillies = System.currentTimeMillis()+10*1000;
        return nextActivationTimeInMillies;
    }

    public boolean onEvaluate(){
        //TODO make proper routine

        //A little short cut to escape the potentially painful eval later on
        if (nextActivationTimeInMillies == 0){
            return false;
        }

        long timeNow = System.currentTimeMillis();
        if (timeNow >= nextActivationTimeInMillies){
            if (isRecurring){
                calculateNextActivationTime();
            } else {
                //if this happens, this condition will never be evaluated true until user sets again
                nextActivationTimeInMillies = 0;
            }
            return true;
        }
        return false;
    }

    public void setNextActivationTimeInMillies(long nextActivationTimeInMillies) {
        this.nextActivationTimeInMillies = nextActivationTimeInMillies;
    }
    
    public long getNextActivationTimeInMillies(){
    	return this.nextActivationTimeInMillies;
    }
    
    public Rule getRule(){
    	return rule;
    }

    public boolean isInPipe() {
        return isInPipe;
    }

    public void setIsInPipe(boolean isInPipe) {
        this.isInPipe = isInPipe;
    }
}
