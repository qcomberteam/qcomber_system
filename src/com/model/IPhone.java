package com.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.dbio.DeviceModelDBIO;

public class IPhone extends DeviceModel {

	public IPhone() {
		// TODO Auto-generated constructor stub
		super("Apple","IPhone","CellPhone");
	}
	
	public void On(){
		System.out.println("Iphone On!");
	}
}
