package com.model;
/**
 * UserRulesManager object belongs to a specific User.
 * UserRulesManager is used to communicate with the DB about rules management via DBIO
 * @author Fuchun Ma
 */

import com.dbio.RuleDBIO;
import com.dbio.Connect;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRulesManager {
	
	//@SuppressWarnings("unused")
	private Rule currentRule; // myUser is only set in the constructor

	/**
	 * Construct UserRulesManager that belongs to a specific user
	 * @param user
	 */
	public UserRulesManager(Rule rule) {
		this.currentRule = rule;
	}

//	public List<Integer> getMyRuleList(Connect con) {
//		List<Integer> myRuleList = new ArrayList<Integer>();
//		try {
//			RuleDBIO getRulesDBIO = new RuleDBIO(con.conn);
//			myRuleList = getRulesDBIO.getAllRules();
//		}
//		return myRuleList;
//	}

	public void addRule(Connect con) throws IllegalArgumentException, SQLException { 
			RuleDBIO addRuleDBIO = new RuleDBIO(con.conn);
			addRuleDBIO.insertRule(this.currentRule);
	}
	
	public void deleteRule(Rule rule) {
		//DBIO.myRuleList.remove(myUser.getMyEmail(), rule);
	}
	
	public Boolean checkRule(Rule rule) {
		List<Integer> myRuleList = new ArrayList<Integer>();
		//myRuleList = DBIO.getRuleList;
		return myRuleList.contains(rule);
	}
	
}
