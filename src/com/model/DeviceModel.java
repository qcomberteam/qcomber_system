package com.model;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.dbio.DeviceModelDBIO;


/**
 * DeviceModel object represents a device model (a device "type"):
 * The brand and the specific model are compulsory fields, e.g. Apple iPhone 6 Plus.
 * 
 * A device model sometimes comes with a model ID, 
 * e.g. iPhone 6 Plus sold in North America all come with Model ID: A1522 (represents the operational frequency).
 * Note that this field should be set by an administrator for user's convenience
 * 
 * Also optionally, the user can set the type of his/her devices by putting his/her iPhone 5 and iPhone 6 Plus under "Cell Phones":
 * this information is stored under the field "myType" (this field can also be set by administrator only.)
 * 
 * @author Hong Zheng
 */
public class DeviceModel {
	// DeviceModel class represents a certain device model, i.e. iPhone 4S, Philips HUE LED.
	
	private String myBrand; // e.g. Apple
	private String myModel; // e.g. iPhone 6 Plus
	
	// The following are optional parameters
	private String type; // e.g. Cell Phones
	private int modelID; 
	
	/**
	 * Constructor when the optional type is given by user
	 * @param brand
	 * @param model
	 * @param type
	 */
	 public DeviceModel(String brand, String model, String type) {
		this.myBrand = brand;
		this.myModel = model;
		this.type = type;
	}
	
	 public void dummyAction(){
		 System.out.println("dummy action activated~~~~");
	 }
	
	//Construct a Model with given model ID;
	public DeviceModel(String brand, String model, String type, int modelID) {
		this(brand, model, type);
		this.modelID = modelID;
	}
	
	
	public String getBrand() {
		return this.myBrand;
	}

	public void setMyBrand(String myBrand) {
		this.myBrand = myBrand;
	}

	public String getModel() {
		return this.myModel;
	}

	public void setMyModel(String myModel) {
		this.myModel = myModel;
	}

	public String getType() {
		return this.type;
	}

	public void setMyType(String type) {
		this.type = type;
	}

	public int getModelID() {
		return this.modelID;
	}
	
//	public static DeviceModel searchModelByID(int modelID){
//		return DeviceModelDBIO.searchByID(modelID);
//	}
	
	public void runAPI(ActionType myActionType){
		Class<?> noparams[] = {};
		Object[] parameters = {};
		Method method;
			try {
				//Dynamically create a method
				method = this.getClass().getDeclaredMethod(myActionType.getActionTypeName(), noparams);
				//execute the method
				method.invoke(this, parameters);

			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				System.out.println("Incorrect Method Name!");
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
