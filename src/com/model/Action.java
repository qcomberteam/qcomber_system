package com.model;
/**
 * Action class is used to ...
 * @author hongzhen
 * @lastUpdate Oct 8, 2015
 */
public class Action {
	// Device class represents a specific device, i.e. William's Black iPhone 6 Plus
	private int ActionID;
	private ActionType myActionType;
	private Device myDevice;
	
	/**
	 * Constructor
	 * @param actionType
	 * @param device
	 */
	public Action(ActionType actionType, Device device) {
		this.myActionType = actionType;
		this.myDevice = device;
	}
	
	/**
	 * constructor used when extracting action object from db;
	 * 
	 * @param actionType
	 * @param device
	 * @param actionID
	 */
	public Action(ActionType actionType, Device device, int actionID){
		this(actionType, device);
		this.ActionID = actionID;
	}

	public ActionType getMyActionType() {
		return myActionType;
	}

	public void setMyActionType(ActionType myActionType) {
		this.myActionType = myActionType;
	}

	public Device getMyDevice() {
		return myDevice;
	}

	public void setMyDevice(Device myDevice) {
		this.myDevice = myDevice;
	}
	
	public int getActionID(){
		return this.ActionID;
	}
	
	public void onExecute() {		
		myDevice.runAPI(myActionType);
	}

    public void execute() {
        onExecute();
    }

}
