package com.model;
/**
 * DeviceModelManager object is implemented for device model management.
 * DeviceModelManager is used to communicate with the DB for device model management via relative DBIOs
 * @author Fuchun Ma
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dbio.DeviceModelDBIO;
import com.dbio.Connect;

public class DeviceModelManager {
	
	// @suppressWarnings("unused");
	private DeviceModel currentDeviceModel; 
	
	/**
	 * Construct DeviceModelManger that is going to be managed
	 * @param currentDeviceModel
	 */
	public DeviceModelManager(DeviceModel deviceModel) {
		this.currentDeviceModel = deviceModel; 
	}
	
	public void addDeviceModel(Connect con) throws IllegalArgumentException, SQLException {
		DeviceModelDBIO addDeviceModelDBIO = new DeviceModelDBIO(con.conn);
		addDeviceModelDBIO.insertModel(this.currentDeviceModel);
	}
	
	public List<DeviceModel> getAllDeviceModelList(Connect con, String brand) throws SQLException {
		
		List<DeviceModel> deviceModelList = new ArrayList<DeviceModel>();
		DeviceModelDBIO getDeviceModelListDBIO = new DeviceModelDBIO(con.conn);
		deviceModelList = getDeviceModelListDBIO.getBrandAllDevices(brand);
		
		return deviceModelList;
	}
}
