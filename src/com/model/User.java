package com.model;
/**
 * User object represents a specific user, it contains that user's identification and optional nickname:
 * @author William Hong
 */
public class User {
	private boolean ifActive;
	private int userID;
	private String myEmail;
	private String myPassword;
	private String myName; // This name is a nickname set by user, it is not used for identification
	
	/**
	 * Constructor when user's name is not given:
	 * Sets the user's email address and password (2 compulsory fields)
	 * @param email
	 * @param password
	 */
	public User(String email, String password) {
		this.myEmail = email;
		this.myPassword = password;
		ifActive = true;
	}

	/**
	 * Constructor when user's name (nickname) is given:
	 * Sets the user's email address, password, and name (nickname)
	 * @param email
	 * @param password
	 * @param name
	 */
	public User(String email, String password, String name) {
		this.myEmail = email;
		this.myPassword = password;
		this.myName = name;
		ifActive = true;
	}

	public String getEmail() {
		return this.myEmail;
	}
	
	public int getUserID(){
		return this.userID;
	}
	
	public String getName() {
		return this.myName;
	}
	
	public String getPW() {
		return this.myPassword;
	}

	public void setMyName(String myName) {
		this.myName = myName;
	}

}
