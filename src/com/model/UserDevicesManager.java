package com.model;
/**
 * UserDevicesManager object belongs to a specific User.
 * UserDevicesManager is used to communicate with the DB about devices management via DBIO
 * @author Hong Zheng and Fuchun Ma
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dbio.DeviceDBIO;
import com.dbio.Connect;

public class UserDevicesManager {

	//@SuppressWarnings("unused")
	private User userForDeviceModel; // myUser is only set in the constructor

	/**
	 * Construct UserDeviceManager that belongs to a specific user
	 * @param userForDeviceModel
	 */
	public UserDevicesManager(User user) {
		this.userForDeviceModel = user;
	}
	
	public List<Device> getMyDeviceList(Connect con) throws SQLException {
		
		List<Device> myDeviceList = new ArrayList<Device>();
		
		DeviceDBIO getDeviceListDBIO = new DeviceDBIO(con.conn);
		myDeviceList = getDeviceListDBIO.getUserAllDevices(this.userForDeviceModel.getUserID());
		
		return myDeviceList;
	}
	
	public void addDevice(Connect con, Device device) throws IllegalArgumentException, SQLException {
			DeviceDBIO addDeviceDBIO = new DeviceDBIO(con.conn);
			addDeviceDBIO.InsertDevice(device);
	}
	
	public void updateDevice(Connect con, Device device) throws IllegalArgumentException, SQLException {
		DeviceDBIO updateDeviceDBIO = new DeviceDBIO(con.conn);
		updateDeviceDBIO.updateDevice(device);
	}
	
	public void deleteDevice(Connect con, Device device) throws IllegalArgumentException, SQLException {
		DeviceDBIO deleteDeviceDBIO = new DeviceDBIO(con.conn);
		deleteDeviceDBIO.DeleteDevice(device);
	}
	
	public Boolean checkDevice(Connect con, Device device) {
		
		DeviceDBIO checkDeviceDBIO = new DeviceDBIO(con.conn);
		
		return checkDeviceDBIO.ifDeviceExists(device);
	}

}
