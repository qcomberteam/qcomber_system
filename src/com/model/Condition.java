package com.model;

import com.util.LogicalOperators;

/**
 * Condition object contains the condition
 * @author hongzhen
 */

public abstract class Condition {
	// Device class represents a specific device, i.e. William's Black iPhone 6 Plus

	private LogicalOperators operator;
	private ConditionType myConditionType;

	private String myParameter;
	
	public Condition(String parameter) {
		//setMyConditionType(conditionType);
		setMyParameter(parameter);
	}

	//public ConditionType getMyConditionType() {
//		return myConditionType;
//	}

//	public void setMyConditionType(ConditionType myConditionType) {
//		this.myConditionType = myConditionType;
//	}

	public String getMyParameter() {
		return myParameter;
	}
	
	public LogicalOperators getConditionOP(){
		return this.operator;
	}
	
	public ConditionType getConditionType(){
		return this.myConditionType;
	}

	public void setMyParameter(String myParameter) {
		this.myParameter = myParameter;
	}
	
	public String toString(){
		
		//TODO: Implement this.
		return null;
	}

    public boolean evaluate(){
        return onEvaluate();
    }

    public abstract boolean onEvaluate();

}
