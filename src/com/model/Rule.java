package com.model;
/**
 * Rule object represents a specific rule
 * A rule object contains an action based on a list of condition(s):
 * 		(1) if there is a single condition: then the list of operator(s) should be empty, the
 *          action is triggered once the condition is met.
 * 		(2) if there are multiple conditions: then the conditions are connected according to the
 * 			list of operator(s), the action is triggered when the connected conditions are met.
 * 
 * e.g.: William's rule: Turn on my HUE LED when time is after 7:00am on Aug 1, 2015 and temperature is 25 oC 
 * @author hongzhen
 * @lastUpdated Oct 8, 2015
 */

import java.util.ArrayList;
import java.util.List;

import com.execute.Main;

public class Rule {
	
	private List<Condition> myConditionList;
	private List<String> myOperatorList;
    private List<TimeBaseCondition> myTBConditionList;
    private List<Condition> myNonTBConditionList;
	// An operator is normally "&&" or "||", thus can in fact be Enum or Boolean
	private Action myAction;
	
	// The following is/are optional parameter(s)
	private String myName; 
	
	/**
	 * Constructor when user does not give the nickname of the rule:
	 * @param conditionList
	 * @param operatorList
	 * @param action
	 */
	public Rule(List<Condition> conditionList, List<String> operatorList, Action action) {
		setMyConditionList(conditionList);
		setMyOperatorList(operatorList);
		setMyAction(action);
		myTBConditionList = new ArrayList<TimeBaseCondition>();
		myNonTBConditionList = new ArrayList<Condition>();
        filterConditions();
	}
	
	/**
	 * Constructor when user gives the nickname of the rule:
	 * @param conditionList
	 * @param operatorList
	 * @param action
	 * @param name
	 */
	public Rule(List<Condition> conditionList, List<String> operatorList, Action action, String name) {
		this(conditionList, operatorList, action);
		setMyName(name);
	}

    private void filterConditions(){
        for (Condition c : myConditionList){
            //not sure if this is tbe best practice, but whatever..
            if (c instanceof TimeBaseCondition){
                myTBConditionList.add((TimeBaseCondition)c);
                //not sure if this is done right...
                Main.TBCPipe.add((TimeBaseCondition)c);                
            } else {
                myNonTBConditionList.add(c);
            }
        }
    }

    public boolean hasTimeBaseConditions(){
        return !myTBConditionList.isEmpty();
    }
    
    public boolean hasNonTimeBaseConditions(){
        return !myNonTBConditionList.isEmpty();
    }

    private boolean evaluateConditions(List<? extends Condition> conditions){
        for (Condition c: conditions){
            if (!c.evaluate()){
                return false;
            }
        }
        return true;
    }

    public boolean evaluateTimeBaseConditions(){
    	for (Condition c: this.myTBConditionList){
            if (c.evaluate()){
                return true;
            }
        }
    	return false;
    }
	
	public boolean evaluateNonTimeBaseConditions(){
        return evaluateConditions(this.myNonTBConditionList);
	}

    public void activate(){
        onActivated();
    }
	
	public void onActivated(){
		this.myAction.execute();
	}

    public void standby(){
        onStandby();
    }
	
	public void onStandby(){
		//TODO not sure what to add, but there you have it
		System.out.println("standing by [DONE]");
	}
	
	
	public List<Condition> getMyConditionList() {
		return myConditionList;
	}
	
	/**
	 * Returns one element of the myConditionList specified by index
	 * 
	 * @param i
	 * @return myConditionList[i]
	 */
	public Condition getConditionByIndex(int i) {
		return this.myConditionList.get(i);
	}

	//this is very dangerous! dont do it!!
	private void setMyConditionList(List<Condition> myConditionList) {
		this.myConditionList = myConditionList;
	}
	
	public void addCondition(Condition c){
		myConditionList.add(c);
		if (c instanceof TimeBaseCondition){
			myTBConditionList.add((TimeBaseCondition) c);
			Main.TBCPipe.add((TimeBaseCondition)c);
		} else {
			myNonTBConditionList.add(c);
		}
	}
	
	//TODO still need bunch of stuffs:
	//for myConditionList, myTBConditionList, and myNonTBConditionList
	//need: byPosition getters
	//need: byPosition remover, byReference remover
	//not sure if we need it now, but ya..
	
	
	public List<String> getMyOperatorList() {
		return myOperatorList;
	}
	
	/**
	 * Concatenate & return all operators into one String with "," as token
	 * 
	 * @return myOperatorList.toString()
	 */
	public String getOperators() {
		return String.join(", ", this.myOperatorList);
	}

	public void setMyOperatorList(List<String> myOperatorList) {
		this.myOperatorList = myOperatorList;
	}

	public Action getMyAction() {
		return myAction;
	}

	public void setMyAction(Action myAction) {
		this.myAction = myAction;
	}

	public String getMyName() {
		return myName;
	}

	public void setMyName(String myName) {
		this.myName = myName;
	}

}
