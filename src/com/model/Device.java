package com.model;
/**
 * Device object represents a specific device, e.g. William's Black iPhone 6 Plus
 * a specific device belongs to a DeviceModel and is identified uniquely by its SerialID
 * Note that this SerialID is set by user and so is possibly incorrect, we need a way to check it 
 * @author hongzhen
 * @LastUpdate Aug 25, 2015
 */

//import java.util.*;

public class Device {
	private int deviceID, userID;
	private DeviceModel myDeviceModel;
	private String mySerialID;
	private String myName;
	
	//Constructor for extracting devices from DB
	public Device(DeviceModel deviceModel, String serialID, int userID, String name, int deviceID){
		this.myDeviceModel = deviceModel;
		this.mySerialID = serialID;
		this.myName = name;
		this.userID = userID;
		this.deviceID = deviceID;
	}
	
	//Constructor when the deviceName (nickname of the device set by user) is not given:
	public Device(DeviceModel deviceModel, String serialID, int userID) {
		setMyDeviceModel(deviceModel);
		setMySerialID(serialID);
		this.userID = userID;
	}
	
	/**
	 * Constructor when the deviceName (nickname of the device set by user) is given:
	 * @param deviceName
	 */
	public Device(DeviceModel deviceModel, String serialID, int userID, String deviceName) {
		this(deviceModel, serialID, userID);
		this.myName = deviceName;
	}
	
	public DeviceModel getMyDeviceModel() {
		return myDeviceModel;
	}

	public void setMyDeviceModel(DeviceModel myDeviceModel) {
		this.myDeviceModel = myDeviceModel;
	}

	public String getMySerialID() {
		return this.mySerialID;
	}

	public void setMySerialID(String mySerialID) {
		this.mySerialID = mySerialID;
	}

	public String getMyName() {
		return myName;
	}

	public void setMyName(String myName) {
		this.myName = myName;
	}
	
	public int getDeviceID(){
		return this.deviceID;
	}
	
	public int getMyDeviceModelID(){
		return this.myDeviceModel.getModelID();
	}

	public int getMyUserID(){
		return this.userID;
	}
	
	public void runAPI(ActionType myActionType){
		myDeviceModel.runAPI(myActionType);
	}

}
