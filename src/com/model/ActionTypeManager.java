package com.model;
/**
 * ActionTypeManager object is implemented for action type management.
 * ActionTypeManager is used to communicate with the DB for action type management via relative DBIOs
 * @author Fuchun Ma
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dbio.ActionTypeDBIO;
import com.dbio.Connect;

public class ActionTypeManager {

	// @suppressWarnings("unused");
	private ActionType currentActionType; 
	
	/**
	 * Construct ActionTypeManger that is going to be managed
	 * @param currentActionType
	 */
	public ActionTypeManager(ActionType actionType) {
		this.currentActionType = actionType; 
	}
	
	public void addActionType(Connect con) throws IllegalArgumentException, SQLException {
		ActionTypeDBIO addActionTypeDBIO = new ActionTypeDBIO(con.conn);
		addActionTypeDBIO.insertActionType(this.currentActionType);
	}
	
	public void deleteActionType(Connect con) throws IllegalArgumentException, SQLException {
		ActionTypeDBIO deleteActionTypeDBIO = new ActionTypeDBIO(con.conn);
		deleteActionTypeDBIO.deleteActionType(this.currentActionType);
	}
	
	public void updateActionType(Connect con) throws IllegalArgumentException, SQLException {
		ActionTypeDBIO updateActionTypeDBIO = new ActionTypeDBIO(con.conn);
		updateActionTypeDBIO.updateActionType(this.currentActionType);
	}
	
	public List<ActionType> getActionTypeList(Connect con) {
		List<ActionType> actionTypeList = new ArrayList<ActionType>();
		try {
			ActionTypeDBIO getActionTypeDBIO = new ActionTypeDBIO(con.conn);
			actionTypeList = getActionTypeDBIO.getModelAllActions(this.currentActionType.getMyDeviceModel());
		} catch(SQLException sqlE) {
			System.out.println("Action Type List cannot be getted" + sqlE.getMessage());
		}
		return actionTypeList;
	}
	
}
