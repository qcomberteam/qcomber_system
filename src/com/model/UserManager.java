package com.model;
/**
 * UserManager object is implemented for user management.
 * UserManager is used to communicate with the DB for user management via user DBIO
 * @author Fuchun Ma
 */

import java.sql.SQLException;

import com.dbio.UserDBIO;
import com.dbio.Connect;

public class UserManager {
	
	// @suppressWarnings("unused");
	private User currentUser; // myUser is only set in the constructor
	
	/**
	 * Construct UserManger that is going to be managed
	 * @param user
	 */
	public UserManager(User user) {
		this.currentUser = user; 
	}
	
	public User getStoredUser(Connect con) throws SQLException{
		
		UserDBIO getUserDBIO = new UserDBIO(con.conn);
		User gettedUser = null;
		
		gettedUser = getUserDBIO.searchByEmail(this.currentUser.getEmail());
		
		return gettedUser;
	}
	
	public void addCurrentUser(Connect con) throws IllegalArgumentException, SQLException {
		UserDBIO addUserDBIO = new UserDBIO(con.conn);
		addUserDBIO.InsertUser(this.currentUser);
	}

	public void avtiveCurrentUser(Connect con) throws IllegalArgumentException, SQLException {
		UserDBIO activeUserDBIO = new UserDBIO(con.conn);
		activeUserDBIO.activeUser(getStoredUser(con));	
	}
	
	public void deAvtiveCurrentUser(Connect con) throws IllegalArgumentException, SQLException {
		UserDBIO deActiveUserDBIO = new UserDBIO(con.conn);
		deActiveUserDBIO.deactUser(getStoredUser(con));;
	}
	
	public void updateCurrentUser(Connect con) throws IllegalArgumentException, SQLException {
		
		UserDBIO updateUserDBIO = new UserDBIO(con.conn);
		// Check if the original password matches in the database, and threw new exception
		if (this.currentUser.getPW() == getStoredUser(con).getPW()) {
			throw new IllegalArgumentException("New password must be different from the original!");
		}
		updateUserDBIO.updateUserPW(getStoredUser(con), this.currentUser.getPW());
	}
}