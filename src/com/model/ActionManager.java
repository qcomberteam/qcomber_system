package com.model;
/**
 * ActionManager object is used for general action management. (Ex, add or delete actions to DB)
 * @author Fuchun Ma
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dbio.ActionDBIO;
import com.dbio.Connect;

public class ActionManager {

	// @suppressWarnings("unused");
	private Action currentAction; 
	
	/**
	 * Construct ActionManager that is going to be managed
	 * @param currentAction
	 */
	public ActionManager(Action action) {
		this.currentAction = action; 
	}
	
	public void addAction(Connect con) throws IllegalArgumentException, SQLException {
			ActionDBIO addActionDBIO = new ActionDBIO(con.conn);
			addActionDBIO.insertAction(this.currentAction);
	}
	
	public void deleteAction(Connect con) throws IllegalArgumentException, SQLException {
			ActionDBIO deleteActionDBIO = new ActionDBIO(con.conn);
			deleteActionDBIO.deleteAction(this.currentAction);
	}
	
	public void updateAction(Connect con) throws IllegalArgumentException, SQLException {
			ActionDBIO updateActionDBIO = new ActionDBIO(con.conn);
			updateActionDBIO.updateAction(this.currentAction);
	}
	
	public List<Action> getActionList(Connect con) {
		List<Action> actionList = new ArrayList<Action>();
		try {
			ActionDBIO getActionDBIO = new ActionDBIO(con.conn);
			getActionDBIO.getDeviceAllActions(this.currentAction.getMyDevice());
		} catch (SQLException sqlE) {
			System.out.println("Action List cannot be getted" + sqlE.getMessage());
		}
		return actionList;
	}
}
