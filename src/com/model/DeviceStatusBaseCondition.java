package com.model;

/**
 * Created by Rane on 2015-08-20.
 */
public class DeviceStatusBaseCondition extends Condition {
    public DeviceStatusBaseCondition(String param){
        super(param);
    }

    @Override
    public boolean onEvaluate(){
        //TODO: do the proper evaluation
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (System.currentTimeMillis()%2 == 0){
            return false;
        }
        return true;
    }
}
