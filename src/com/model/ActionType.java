package com.model;
/**
 * @author William Zheng
 * @lastUpdate Sept 3, 2015
 */

public class ActionType {
	// Device class represents a specific device, i.e. William's Black iPhone 6 Plus

	private DeviceModel myDeviceModel;
	private String actionTypeName;
	private int actionType_ID;
		
	public ActionType(DeviceModel deviceModel, String actionTypeName) {
		this.myDeviceModel = deviceModel;
		this.actionTypeName = actionTypeName;
	}
	
	public DeviceModel getMyDeviceModel() {
		return this.myDeviceModel;
	}

	public void setMyDeviceModel(DeviceModel myDeviceModel) {
		this.myDeviceModel = myDeviceModel;
	}

	public String getActionTypeName() {
		return this.actionTypeName;
	}

	public void setActionTypeName(String actionTypeName) {
		this.actionTypeName = actionTypeName;
	}
	
	public int getActionTypeID(){
		return this.actionType_ID;
	}

}
