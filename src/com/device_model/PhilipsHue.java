package com.device_model;

import com.model.DeviceModel;

public class PhilipsHue extends DeviceModel {
	
	/**
	 * Constructor when the optional type is given by user
	 * @param serialNumber
	 */
	public PhilipsHue(String brand, String model, String type)
	{
		super(brand, model, type);
	}
	
	public void ledOn()
	{
		// Philips LED On API
	}
	
	public void ledOff()
	{
		// Philips LED On API
	}

}
