package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.dbio.Connect;
import com.model.*;
import com.util.DatabaseConfig;

@Path("/services")
public class CoreREST {
	private static Connect con;
	
	@PostConstruct
	public void dBConnectConstructor() {

		try {
		con = new Connect(DatabaseConfig.DEVELOPMENT_URL, 
                		  DatabaseConfig.DEVELOPMENT_USER, 
                          DatabaseConfig.DEVELOPMENT_PASS);
		} catch(SQLException sqlE) {
			System.out.println("DB cannot be connected" + sqlE.getMessage());
		} catch(ClassNotFoundException cnfE) {
			System.out.println("DB Connention class cannot be found!");
		}
				
	}
	
	@Path("/greetings")
	@GET
	@Produces("application/json")
	public Response convertFtoC() throws JSONException{

		String result = "Hello World!";
		
		System.out.println(result);

		return Response.status(200).entity(result).build();
	}
	

	@Path("/degree/{f}&{g}")
	@GET
	@Produces("application/json")
	public Response convertFtoCfromInput(@PathParam("f") float f, @PathParam("g") float g)
			throws JSONException {

		JSONObject jsonObject = new JSONObject();
		float celsius;
		celsius = (f - g - 32) * 5 / 9;
		jsonObject.put("F Value", f);
		jsonObject.put("G Value", g);
		jsonObject.put("C Value", celsius);

		String result = "@Produces(\"application/json\") Output: \n\nF to C Converter Output: \n\n"
				+ jsonObject;
		return Response.status(200).entity(result).build();
	}

	@Path("/login/email&password")
	@GET
	@Produces("application/json")
	public Response loginAccount(@PathParam("email") String email, @PathParam("password") String password) throws JSONException {
		
		//Call DBIO functions
		
		return null;
	}
	
	@Path("/user/add")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doPostNewUser(InputStream newUserStream) {
		// need to build proper responses in call cases
		StringBuilder newUserBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(newUserStream));
			String line = null;
			while ((line = in.readLine()) != null) {
				newUserBuilder.append(line);
			}
			
			// Parsing JSON object and create an user manager for adding
			UserManager addUserMana = userManagerBuilder(newUserBuilder.toString());
			
			// Add new user to database via User Manager
			addUserMana.addCurrentUser(con);
			
			System.out.println("Data Received: " + newUserBuilder.toString());
		} catch (IOException ioE) {
			System.out.println("Error Parsing the new user input stream!");
		} catch (IllegalArgumentException ilaA) {
			System.out.println("User is already existed!");
		} catch (SQLException sqlE) {
			System.out.println("User cannot be added to database" + sqlE.getMessage());
		} finally {
			con.close();
		}
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity(newUserBuilder.toString()).build();
	}
	
	@Path("/user/active")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doAcitveUser(InputStream activeUserStream) {
		// need to build proper responses in call cases and DBIO for user update not working right now
		StringBuilder activeUserBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(activeUserStream));
			String line = null;
			while ((line = in.readLine()) != null) {
				activeUserBuilder.append(line);
			}
			
			// Parsing JSON object and create an user manger for active
			UserManager activeUserMana = userManagerBuilder(activeUserBuilder.toString());
			
			// Active user in database via User Manager
			activeUserMana.avtiveCurrentUser(con);
			
			System.out.println("Data Received: " + activeUserBuilder.toString());
		} catch (IOException ioE) {
			System.out.println("Error Parsing the active user input stream!");
		} catch (IllegalArgumentException ilaA) {
			System.out.println("Users does not exist!");
		} catch (SQLException sqlE) {
			System.out.println("User cannot be active in database" + sqlE.getMessage());
		} finally {
			con.close();
		}
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(activeUserBuilder.toString()).build();
	}
	
	@Path("/user/deactive")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doDeacitveUser(InputStream deactiveUserStream) {
		// need to build proper responses in call cases and DBIO for user update not working right now
		StringBuilder deactiveUserBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(deactiveUserStream));
			String line = null;
			while ((line = in.readLine()) != null) {
				deactiveUserBuilder.append(line);
			}
			
			// Parsing JSON object and create an user manager for deactive
			UserManager deactiveUserMana = userManagerBuilder(deactiveUserBuilder.toString());
			
			// Deactive user in database via User Manager
			deactiveUserMana.deAvtiveCurrentUser(con);
			
			System.out.println("Data Received: " + deactiveUserBuilder.toString());
		} catch (IOException ioE) {
			System.out.println("Error Parsing the active user input stream!");
		} catch (IllegalArgumentException ilaA) {
			System.out.println("Users does not exist!");
		} catch (SQLException sqlE) {
			System.out.println("User cannot be deactive in database" + sqlE.getMessage());
		} finally {
			con.close();
		}
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(deactiveUserBuilder.toString()).build();
	}
	
	@Path("/user/update")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doPutUser(InputStream updateUserStream) {
		// need to build proper responses in call cases and DBIO for user update not working right now
		StringBuilder updateUserBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(updateUserStream));
			String line = null;
			while ((line = in.readLine()) != null) {
				updateUserBuilder.append(line);
			}
			
			// Parsing JSON object and create an user manager for update
			UserManager updateUserMana = userManagerBuilder(updateUserBuilder.toString());
			
			// Update user to database via User Manager
			updateUserMana.updateCurrentUser(con);
			
			System.out.println("Data Received: " + updateUserBuilder.toString());
		} catch (IOException ioE) {
			System.out.println("Error Parsing the update user input stream!");
		} catch (IllegalArgumentException ilaA) {
			System.out.println("Users does not exist!");
		} catch (SQLException sqlE) {
			System.out.println("User cannot be updated to database" + sqlE.getMessage());
		} finally {
			con.close();
		}
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(updateUserBuilder.toString()).build();
	}
	
	public UserManager userManagerBuilder(String userString) throws JSONException {
		// Parsing JSON object and create a user object
		JSONObject incomingJson = new JSONObject(userString);
		JSONObject updateUserJson = incomingJson.getJSONObject("user");
		
		String email = updateUserJson.getString("myEmail");
		String pwd = updateUserJson.getString("myPwd");
		String name = updateUserJson.getString("myName");
		
		// Build the user manager
		User newUser = new User(email, pwd, name);
		UserManager newUserManager = new UserManager(newUser);
		
		return newUserManager;
	}

	@Path("/devicemodel/add")
	@POST
	@Consumes("application/json")
	public void doPostNewDeviceModel(HttpServletRequest request, HttpServletResponse response)
			throws  ServletException, IOException, JSONException {
		
		// Ask how to get model ID
		String brand = request.getParameter("myBrand");
		String model = request.getParameter("myModel");
		String type = request.getParameter("type");
		
		DeviceModel newDeviceModel = new DeviceModel(brand, model, type);
	    postDeviceModel( newDeviceModel );
	
	}
	
    private Response postDeviceModel( DeviceModel newDeviceModel ) {
	// Connect to DB
	// Call insertDeviceModel DBIO
	String output = "New device model added successfully: " + newDeviceModel.getBrand() + newDeviceModel.getModel();
	return Response.status(200).entity(output).build();
}
	
	@Path("/device/add")
	@POST
	@Consumes("application/json")
	public void doPostNewDevice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, JSONException {
		  
		JSONObject jSONDeviceModel = new JSONObject(request.getParameter("myDeviceModel"));
	    String serialID = request.getParameter("mySerialID");
	    String deviceNickName = request.getParameter("myName");
	    int deviceID = Integer.parseInt(request.getParameter("myDeviceID"));
	    int userID = Integer.parseInt(request.getParameter("myUserID"));
	    
	    // Remember to ask model ID
	    DeviceModel currentDeviceModel = new DeviceModel(jSONDeviceModel.getString("myBrand"),
	    												 jSONDeviceModel.getString("myModel"),
	    												 jSONDeviceModel.getString("type"));
	    
	    // Check if the device nickname is given
	    if (deviceNickName != null) {
	    	Device newDevice = new Device(currentDeviceModel, serialID, userID, deviceNickName);
	    	postDevice( newDevice );
	    } else {
	    	Device newDevice = new Device(currentDeviceModel, serialID, userID);
	    	postDevice( newDevice );
	    }
	    
	}
	
    private Response postDevice( Device newDevice ) {
    	String output = "New device added Successfully: " + newDevice.getMySerialID();
    	return Response.status(200).entity(output).build();
    }
  
}