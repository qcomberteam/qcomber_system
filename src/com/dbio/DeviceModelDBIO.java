package com.dbio;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.model.DeviceModel;

/**
 * DBIO for Model table;
 * 
 * @author Johnny Jiang
 * @lastUpdate Sept 17, 2015
 */
public class DeviceModelDBIO{
	private Connection con = null;
	
	public DeviceModelDBIO(Connection con){
		this.con = con;
	}
	
	public void insertModel(DeviceModel model) throws SQLException{		
			if(ifModelExists(model) == true)
				throw new IllegalArgumentException("Device model already exists!");
			
			PreparedStatement ps = null;
			String query = "INSERT INTO Model (brand, model)"
							+ "VALUES (?,?)";
			try{
				ps = con.prepareStatement(query);
				ps.setString(1, model.getBrand().toLowerCase());
				ps.setString(2, model.getModel().toLowerCase());
				ps.executeUpdate();
			} catch(SQLException e){
				e.printStackTrace();
			} finally {
				if(ps != null)
					ps.close();
			}
		}
	
	//Check if a device model exists based on brand name and model
	private boolean ifModelExists(DeviceModel model) throws SQLException {
		String query = "SELECT count(*) FROM Model WHERE brand = ? AND model = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, model.getBrand().toLowerCase());
			ps.setString(2, model.getModel().toLowerCase());
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt(1) != 0)
				return true;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
		return false;	
	}
	
	/**
	 * returns all device models with the same given brand
	 * @param brand
	 * @return ArrayList<DeviceModel>
	 * @throws SQLException
	 */
	public ArrayList<DeviceModel> getBrandAllDevices(String brand) throws SQLException{
		String query = "SELECT * FROM Model WHERE brand = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<DeviceModel> resultDeviceModelList = new ArrayList<DeviceModel>();
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, brand.toLowerCase());
			rs = ps.executeQuery();
			while(rs.next()){
				String model = rs.getString("model");
				String type = rs.getString("modelType");
				DeviceModel nxtModel = new DeviceModel(brand, model, type);
				resultDeviceModelList.add(nxtModel);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
		return resultDeviceModelList;
	}
	
	/**
	 * return a deviceModel Object given a modelID
	 * @param modelID
	 * @return DeviceModel
	 */
	public DeviceModel searchByID(int modelID) {
		String query = "SELECT * FROM Model WHERE modelID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		DeviceModel retModel = null;
		
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, modelID);
			rs = ps.executeQuery();
			while(rs.next()){
				String brand = rs.getString("brand");
				String model = rs.getString("model");
				String type = rs.getString("modelType");
				retModel = new DeviceModel(brand, model, type);
			}
			ps.close();
		} catch (SQLException e){
			e.printStackTrace();
		} 
		return retModel;
	}
}