package com.dbio;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.model.Device;
import com.model.Rule;

public class RuleDBIO{
	private Connection con = null;
	
	public RuleDBIO(Connection con){
		this.con = con;
	}
	
	public void insertRule(Rule rule) throws SQLException{
		if(ifRuleExists(rule) == true)
			throw new IllegalArgumentException("Rule already exists");
		
		PreparedStatement ps = null;
		String query = "INSERT INTO Rule"
				+ "(name, operators, conditionOP1, conditionType1, conditionPara1, "
				+ "conditionOP2, conditionType2, conditionPara1, conditionOP3, "
				+ "conditionType3, conditionPara3, actionID) VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, rule.getMyName());
			ps.setString(2, rule.getOperators());
			for(int i=0; i<3; i++){
				ps.setString(3+i*3, rule.getConditionByIndex(i).getConditionOP().toString());
				ps.setString(4+i*3, rule.getConditionByIndex(i).getConditionType().toString());
				ps.setString(5+i*3, rule.getConditionByIndex(i).getMyParameter().toString());
			}
			ps.setInt(12, rule.getMyAction().getActionID());
			ps.executeUpdate();
		} catch(SQLException e){
			System.out.println(e.getMessage());
		} finally {
			if(ps != null)
				ps.close();	
		}
	}
	

	private boolean ifRuleExists(Rule rule) {
		// TODO Auto-generated method stub
		return false;
	}

	public void getAllRules() throws SQLException{
		String query = "SELECT * FROM Rule";
		PreparedStatement ps = null;
		ResultSet rs = null;
		//ArrayList<Device> resultDeviceList = new ArrayList<Device>();
		try{
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()){
				//to-do
				for(int i = 1; i < 14; i++)
			        System.out.print(rs.getString(i) + " ");
			    System.out.println();
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(rs != null)
				rs.close();
			if(ps != null)
				ps.close();
		}
	}
}