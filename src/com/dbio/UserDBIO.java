package com.dbio;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.User;
import com.sun.jersey.api.NotFoundException;

/**
 * DBIO for User table;
 * 
 * @author Johnny Jiang
 * @lastUpdate Sept 24, 2015
 *
 */
public class UserDBIO{
	private Connection con = null;
	
	public UserDBIO(Connection con){
		this.con = con;
	}
	
	/**
	 * Insert a new user after checking the same user has not been registered
	 * @param user
	 */
	public void InsertUser(User user) throws SQLException{				
		if(ifUserExists(user) == true)
			throw new IllegalArgumentException("User already exists");
		
		PreparedStatement ps = null;
		String query = "INSERT INTO Users"
				+ "(name, pw, email) VALUES"
				+ "(?,?,?)";
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, user.getName());
			ps.setString(2, user.getPW());
			ps.setString(3, user.getEmail());
			ps.executeUpdate();
			ps.close();			
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	/**
	 * Activated an user account
	 * @param user
	 */
	public void activeUser(User user) throws SQLException{
		if(ifUserExists(user) == false)
			throw new IllegalArgumentException("User does not exist!");
		
		PreparedStatement ps = null;
		String query = "UPDATE Users SET ifActive = ? WHERE uid = ?";
		try{
			ps = con.prepareStatement(query);
			ps.setBoolean(1, true);
			ps.setInt(2, user.getUserID());
			ps.executeUpdate();
			ps.close();		
			System.out.println("User has been activated!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	/**
	 * Deactivate an user account
	 * @param user
	 */
	public void deactUser(User user) throws SQLException{
		if(ifUserExists(user)==false)
			throw new IllegalArgumentException("User does not exist!");
		
		PreparedStatement ps = null;
		String query = "UPDATE Users SET ifActive = ? WHERE uid = ?";
		try{
			ps = con.prepareStatement(query);
			ps.setBoolean(1, false);
			ps.setInt(2, user.getUserID());
			ps.executeUpdate();
			ps.close();		
			System.out.println("User has been deactivated!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	/**
	 * Update user password if the user record exists
	 * @param user
	 * @param newPW
	 */
	public void updateUserPW (User user, String newPW) throws SQLException{
		if(ifUserExists(user) == false)
			throw new IllegalArgumentException("Users does not exist!");
		
		PreparedStatement ps = null;		
		String query = "UPDATE User SET pw = ? WHERE uid = ?";
				
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, newPW);
			ps.setInt(2, user.getUserID());
			ps.executeUpdate();
			ps.close();		
			System.out.println("Record has been updated!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	/**
	 * Check if the given user record already exists in DB;
	 * Return true if record exists
	 * 
	 * @param user
	 * @return True/False
	 */
	public boolean ifUserExists(User user) throws SQLException{	
		String query = "SELECT count(*) FROM Users WHERE Email = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, user.getEmail());
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt(1) != 0)
				return true;
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(rs != null)
				rs.close();
			if(ps != null)
				ps.close();
		}
		return false;	
	}
	
	
	/**
	 * returns a User based on given email; raise a NotFoundException if no record match
	 * @param email
	 * @return User object
	 * @throws SQLException, NotFoundException
	 */
	public User searchByEmail(String email) throws SQLException{
		String query = "SELECT * FROM Users WHERE Email = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, email);
			rs = ps.executeQuery();
			while(rs.next()){
				user = new User(email, rs.getString("pw"), rs.getString("name"));
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally{
			if(rs != null)
				rs.close();
			if(ps != null)
				ps.close();
		}
		
		if(user != null){
			return user;
		} else {
			throw new NotFoundException("User not found!");
		}
	}
}