package com.dbio;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.model.ActionType;
import com.model.DeviceModel;

/**
 * DBIO for ActionType table;
 * 
 * @author Johnny Jiang
 * @lastUpdated Sept 24, 2015
 */
public class ActionTypeDBIO{
	private Connection con = null;
	
	public ActionTypeDBIO(Connection con){
		this.con = con;
	}
	
	public void insertActionType(ActionType actionType)
		throws SQLException{		
		if(ifActionTypeExists(actionType) == true)
			throw new IllegalArgumentException("Action type already exists!");
		
		PreparedStatement ps = null;
		String query = "INSERT INTO ActionType (modelID, modelActionType)"
						+ "VALUES (?,?)";
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, actionType.getMyDeviceModel().getModelID());
			ps.setString(2, actionType.getActionTypeName());
			ps.executeUpdate();
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	public void deleteActionType(ActionType actionType) throws SQLException{
		if(ifActionTypeExists(actionType) == false)
			throw new IllegalArgumentException("Action type does not exist!");
		
		PreparedStatement ps = null;
		String query = "DELETE ActionType WHERE modelActionTypeID = ?";
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, actionType.getActionTypeID());
			ps.executeUpdate();
			System.out.println("Action type has been deleted!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	public void updateActionType(ActionType actionType) throws SQLException{
		if(ifActionTypeExists(actionType) == false)
			throw new IllegalArgumentException("Action type does not exist!");
		
		PreparedStatement ps = null;		
		String query = "UPDATE ActionType SET modelID = ?, modelActionType = ?";
				
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, actionType.getMyDeviceModel().getModelID());
			ps.setString(2, actionType.getActionTypeName());
			ps.executeUpdate();	
			System.out.println("Record has been updated!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();	
		}
	}
	
	public boolean ifActionTypeExists(ActionType actionType) {
		String query = "SELECT count(*) FROM ActionType WHERE modelID = ? AND modelActionType = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, actionType.getMyDeviceModel().getModelID());
			ps.setString(2, actionType.getActionTypeName());
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt(1) != 0)
				ps.close();
				return true;
		} catch (SQLException e){
			System.out.println(e.getMessage());
		}
		return false;		
	}
	
	
	/**
	 * returns all action types available to a given model
	 * @param deviceModel
	 * @return ArrayList<ActionType>
	 * @throws SQLException
	 */
	public ArrayList<ActionType> getModelAllActions(DeviceModel deviceModel) throws SQLException{
		String query = "SELECT * FROM ActionType WHERE modelID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ActionType> resultActionTypeList = new ArrayList<ActionType>();
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, deviceModel.getModelID());
			rs = ps.executeQuery();
			while(rs.next()){
				String actionTypeName = rs.getString("modelActionType");
				ActionType nxtType = new ActionType(deviceModel, actionTypeName);
				resultActionTypeList.add(nxtType);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
		return resultActionTypeList;
	}
	
	/**
	 * return an actionType object given an actionType ID
	 * @param actionTypeID
	 * @return
	 */
	public ActionType searchByID(int actionTypeID) {
		String query = "SELECT * FROM ActionType WHERE modelActionTypeID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ActionType retActionType = null;
		DeviceModelDBIO modeldbio = null;
		 
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, actionTypeID);
			rs = ps.executeQuery();
			while(rs.next()){
				int modelID = rs.getInt("modelID");
				String modelActionType = rs.getString("modelActionType");
				retActionType = new ActionType(modeldbio.searchByID(modelID), modelActionType);
			}
			ps.close();
		} catch (SQLException e){
			e.printStackTrace();
		} 
		return retActionType;
	}
}