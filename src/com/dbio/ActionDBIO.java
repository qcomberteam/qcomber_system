package com.dbio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.model.Action;
import com.model.ActionType;
import com.model.Device;

/**
 * DBIO for Action table;
 * @author Johnny Jiang
 * @lastUpdate Sept 24, 2015
 * 
 */
public class ActionDBIO{
	private Connection con = null;
	
	public ActionDBIO(Connection con){
		this.con = con;
	}
	
	public void insertAction(Action action)
			throws SQLException{		
			if(ifActionExists(action) == true)
				throw new IllegalArgumentException("Action already exists!");
			
			PreparedStatement ps = null;
			String query = "INSERT INTO Action (deviceID, modelActionType)"
							+ "VALUES (?,?)";
			try{
				ps = con.prepareStatement(query);
				ps.setInt(1, action.getMyDevice().getDeviceID());
				ps.setInt(2, action.getMyActionType().getActionTypeID());
				ps.executeUpdate();
			} catch(SQLException e){
				e.printStackTrace();
			} finally {
				if(ps != null)
					ps.close();
			}
		}

	public boolean ifActionExists(Action action) throws SQLException {
		String query = "SELECT count(*) FROM Action WHERE deviceID = ? AND ActionTypeID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, action.getMyDevice().getDeviceID());
			ps.setInt(2, action.getMyActionType().getActionTypeID());
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt(1) != 0)
				ps.close();
				return true;
		} catch (SQLException e){
			System.out.println(e.getMessage());
		} finally {
			if(ps != null)
				ps.close();
		}
		return false;
	}
	
	public void deleteAction(Action action) throws SQLException{
		if(ifActionExists(action) == false)
			throw new IllegalArgumentException("Action does not exist!");
		
		PreparedStatement ps = null;
		String query = "DELETE Action WHERE modelActionTyID = ? AND deviceID = ?";
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, action.getMyActionType().getActionTypeID());
			ps.setInt(2, action.getMyDevice().getDeviceID());
			ps.executeUpdate();
			System.out.println("Action has been deleted!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	public void updateAction(Action action) throws SQLException{
		if(ifActionExists(action) == false)
			throw new IllegalArgumentException("Action does not exist!");
		
		PreparedStatement ps = null;		
		String query = "UPDATE Action SET deviceID = ?, modelActionTypeID = ?";
				
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, action.getMyDevice().getDeviceID());
			ps.setInt(2, action.getMyActionType().getActionTypeID());
			ps.executeUpdate();	
			System.out.println("Record has been updated!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();	
		}
	}
	
	/**
	 * Return all the actions available to the given device
	 * 
	 * @param device
	 * @return ArrayList<Action>
	 * @throws SQLException
	 */
	public ArrayList<Action> getDeviceAllActions(Device device) throws SQLException{
		String query = "SELECT * FROM Action WHERE deviceID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Action> resultActionList = new ArrayList<Action>();
		
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, device.getDeviceID());
			rs = ps.executeQuery();
			ActionTypeDBIO actionTypeDBIO = new ActionTypeDBIO(con);
			while(rs.next()){
				int actionTypeID = rs.getInt("modelActionTypeID");
				ActionType actionType = actionTypeDBIO.searchByID(actionTypeID);
				Action nxtAction = new Action(actionType, device);
				resultActionList.add(nxtAction);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(rs != null)
				rs.close();
			if(ps != null)
				ps.close();
		}
		return resultActionList;
	}
	
}