package com.dbio;
import java.sql.*;



//Connection class to the database;

public class Connect {
	
	private final String driver = "com.mysql.jdbc.Driver";
	
	public Connection conn;
	
	//Connect to database	
	public Connect(String url, String user, String password) throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		conn = DriverManager.getConnection(url, user, password);
	}
	
	//Close database	
	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			/* ignored */
		}
	}
}