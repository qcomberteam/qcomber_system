package com.dbio;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.model.Device;
import com.model.DeviceModel;

/**
 * DBIO for Device table;
 * 
 * @author Johnny Jiang
 * @LastUpdate Aug 25, 2015
 */
public class DeviceDBIO{
	private Connection con = null;
	
	public DeviceDBIO(Connection con){
		this.con = con;
	}
	
	/**
	 * Create a new device record;
	 * @param device
	 * @param user
	 * @param model
	 * @throws SQLException
	 */
	public void InsertDevice(Device device)
		throws SQLException{		
		if(ifDeviceExists(device) == true)
			throw new IllegalArgumentException("Device already exists!");
		
		PreparedStatement ps = null;
		String query = "INSERT INTO Device (uid, modelID, serialID, name)"
						+ "VALUES (?,?,?,?)";
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, device.getMyUserID());
			ps.setInt(2, device.getMyDeviceModelID());
			ps.setString(3, device.getMySerialID());
			ps.setString(4, device.getMyName());
			ps.executeUpdate();
			
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	/**
	 * Delete a device record;
	 * @param device
	 * @throws SQLException
	 */
	public void DeleteDevice(Device device) throws SQLException{
		if(ifDeviceExists(device)==false)
			throw new IllegalArgumentException("Device does not exist!");
		
		PreparedStatement ps = null;
		String query = "DELETE Device WHERE did = ?";
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, device.getDeviceID());
			ps.executeUpdate();
			
			System.out.println("Device has been deleted!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
	}
	
	/**
	 * Update all device fields except for deviceID and UserID
	 * @param device
	 */
	public void updateDevice(Device device) throws SQLException{
		if(ifDeviceExists(device)==false)
			throw new IllegalArgumentException("Device does not exist!");
		
		PreparedStatement ps = null;		
		String query = "UPDATE Device SET modelID = ?, serialID = ?, name = ? WHERE did = ?";
				
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, device.getMyDeviceModelID());
			ps.setString(2, device.getMySerialID());
			ps.setString(3, device.getMyName());
			ps.setInt(4, device.getDeviceID());
			ps.executeUpdate();	
			System.out.println("Record has been updated!");
		} catch(SQLException e){
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();	
		}
	}
	
	/**
	 * Return an arraylist of devices that belonged to the given user
	 * 
	 * @param user
	 * @return ArrayList<Device>
	 * @throws SQLException
	 */
	public ArrayList<Device> getUserAllDevices(int userID) throws SQLException{
		String query = "SELECT * FROM Device WHERE uid = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Device> resultDeviceList = new ArrayList<Device>();
		try{
			ps = con.prepareStatement(query);
			ps.setInt(1, userID);
			rs = ps.executeQuery();
			DeviceModelDBIO deviceModelDBIO = new DeviceModelDBIO(con);
			while(rs.next()){
				int id = rs.getInt("did");
				//User Deviceuser = UserDBIO.searchByID(rs.getInt("uid"));
				DeviceModel model = deviceModelDBIO.searchByID(rs.getInt("modelID"));
				String serialID = rs.getString("serialID");
				String name = rs.getString("name");
				Device nxtDevice = new Device(model, serialID, userID, name, id);
				resultDeviceList.add(nxtDevice);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(ps != null)
				ps.close();
		}
		return resultDeviceList;
	}
	
	/**
	 * Return true if the given device record already exists in db
	 * based on the serialID & modelID of the device.
	 * 
	 * @param device
	 * @return true/false
	 */
	public boolean ifDeviceExists(Device device){
		String query = "SELECT count(*) FROM Device WHERE serialID = ? AND modelID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = con.prepareStatement(query);
			ps.setString(1, device.getMySerialID());
			ps.setInt(2, device.getMyDeviceModelID());
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt(1) != 0)
				ps.close();
				return true;
		} catch (SQLException e){
			e.printStackTrace();
		}
		return false;	
	}
}